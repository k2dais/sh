import React from 'react'
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom'
import Home from '../views/Home'
import Login from '../views/Login'
import Auth from '../views/Auth'
import Shopping from '../views/auth/Shopping'
import Center from '../views/auth/Center'
import Notfound from '../views/Notfound'
import Category from '../views/Category'
import Detail from '../views/Detail'
import Changecenter from '../views/auth/Changecenter'
import Register from '../views/Register'

export default function Index() {
  return (
    <BrowserRouter initialEntries={['/auth/home']}>
        <Routes>
            <Route path='/' element={<Navigate to='/auth/home'/>}/>
            <Route path='/login' element={<Login/>}/>
            <Route path='/register' element={<Register/>}/>
            <Route path='/auth' element={
              <Authjudge>
                <Auth/>
              </Authjudge>
            }>
              <Route path='' element={<Navigate to='/auth/home'/>}/>
              <Route path='/auth/home' element={<Home/>}/>
              <Route path='/auth/detail/:id' element={<Detail/>}/>
              <Route path='/auth/category' element={<Category/>}/> 
              <Route path='/auth/shopping' element={<Shopping/>}/>
              <Route path='/auth/center' element={<Center/>}/>
              <Route path='/auth/changecenter' element={<Changecenter/>}/>
              <Route path='*' element={<Notfound />}/>
            </Route>
        </Routes>
    </BrowserRouter>
  )
}

const Authjudge = ({children}) =>{
  let token = localStorage.getItem('token');
  if(token)
    return children;
  else
    return <Navigate to='/login'/>
}
