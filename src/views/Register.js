import React, { useRef } from 'react'
import {
  Form,
  Input,
  Button,
  Radio,
  Toast,
  NavBar
} from 'antd-mobile'
import axios from 'axios';

export default function Changecenter() {
  const ref = useRef();
  const onFinish = (values) => {
    console.log(values);
    Toast.show("创建新用户成功")
    axios.post(`/users`,{
        username: values.username,
        password: values.password,
        gender: values.gender,
        address: values.address?values.national:'',
        country: values.national?values.national:'',
        city: values.city?values.city:'',
        tel: values.tel?values.tel:null
    })
  }
  return (
    <div style={{paddingBottom:'3.0625rem'}}>
      <NavBar onBack={()=>{
        window.history.back();
      }}>注册新用户</NavBar>
      <Form
        layout='horizontal'
        onFinish={onFinish}
        ref = {ref}
        footer={
          <Button 
            block 
            type='submit' 
            color='primary' 
            size='large'
           >
            提交
          </Button>
        }
      >
        <Form.Item
          name='username'
          label='昵称'
          rules={[{ required: true, message: '昵称是必须的' }]}
        >
          <Input placeholder='输入一个可爱的昵称吧~' />
        </Form.Item>
        <Form.Item
          name='password'
          label='密码'
          rules={[{ required: true, message: '密码是必须的' }]}
        >
          <Input type='password' placeholder='不要把密码告诉别人哦！' />
        </Form.Item>
        <Form.Item 
            name='gender' 
            label='性别'
            rules={[{ required: true, message: '必须选一个性别' }]}
        >
            <Radio.Group>
                <Radio value='0'>男&emsp;</Radio>
                <Radio value='1'>女&emsp;</Radio>
                <Radio value='2'>第三性别</Radio>
            </Radio.Group>
        </Form.Item>
        <Form.Item name='national' label='国籍' help='所属国家'>
          <Input placeholder='请输入国家' />
        </Form.Item>
        <Form.Item name='city' label='城市' help='居住城市'>
          <Input placeholder='请输入城市' />
        </Form.Item>
        <Form.Item name='address' label='地址' help='详情地址'>
          <Input placeholder='请输入地址' />
        </Form.Item>
        <Form.Item 
            name='tel' 
            label='电话'
            rules={[{ min:11, max:11, message: '电话是11位数字哦~' }]}
        >
          <Input type='number' placeholder='输入你的电话，11位数字' />
        </Form.Item>
      </Form>
    </div>
  )
}
