import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { Swiper, SearchBar} from 'antd-mobile'
import { useNavigate } from 'react-router-dom';

export default function Home() {
  const [swiPic, setSwiPic] = useState();
  const [list, setList] = useState([]);
  const [goodlist, setGoodList] = useState([]);
  const navigate = useNavigate();
  useEffect(()=>{
    Promise.all([
      axios('/recommends'),
      axios('/goods'),
    ]).then(res=>{
      setSwiPic(res[0].data);
      console.log(res[1].data)
      setList(res[1].data);
      setGoodList(res[1].data);
    })
  },[])
  const filtergoodslist = (value) =>{
    // eslint-disable-next-line eqeqeq
    if(value ==='')
      setGoodList(list);
    else
      setGoodList(list.filter(item => item.title.toUpperCase().includes(value.toUpperCase())
      ||item.feature.toUpperCase().includes(value.toUpperCase())));
  }

  return (
    <div>
      <Swiper autoplay loop>
        {
          swiPic&& swiPic.map(item => 
            <Swiper.Item key={item.id}>
              <img style={{width:'100%'}} src={`http://localhost:5000/${item.url}`} alt='加载失败'/>
            </Swiper.Item>
          )
        }
      </Swiper>
      <SearchBar style={{margin:'.3rem .3rem'}} placeholder='请输入内容'  showCancelButton  onChange={(value)=>{
        filtergoodslist(value)
      }}/>
        <div style={{paddingBottom:'3.0625rem'}}>
          {
            goodlist.map(item =>
            <div key={item.id} style={{display:'flex', justifyContent:'space-around', margin:'1rem 1rem'}} onClick={()=>{
              navigate(`/auth/detail/${item.id}`)
            }}>
              <img src={`http://localhost:5000/${item.poster}`} alt='加载失败' style={{width:'5rem', height:'6rem'}}/>
              <div style={{marginLeft:'2rem', fontSize:'1.1rem', flex:1, display:'flex', flexDirection:'column',justifyContent:'space-around'}}>
                <span style={{fontSize:'1.1rem'}}>{item.title}</span>
                <span style={{color:'red'}}>￥{item.price}</span>
                <span>好评率：{item.goodcomment}</span>
              </div>
            </div>)
          }
        </div>
    </div>
  )
}
