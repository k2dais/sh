import React, { useEffect, useRef } from 'react'
import { Avatar, Image } from 'antd';
import { Form, Button,Input } from 'antd-mobile';
import { useNavigate } from 'react-router-dom';
import { connect } from 'react-redux';
import getCenterList from '../../redux/createActor/getCenterList';

function Center(props) {
  let { personalList, getCenterList } = props;
  let {username} = JSON.parse(localStorage.getItem('token'));
  const ref = useRef();
  const navigate = useNavigate();
  const genderList = ['男', '女', '第三性别'];
  useEffect(()=>{
    console.log(personalList);
    if(personalList.length === 0){
      getCenterList();
    }
    else{
      ref.current.setFieldsValue({
        country: personalList[0].country,
        gender: genderList[personalList[0].gender],
        city: personalList[0].city,
        tel: personalList[0].tel,
        address: personalList[0].address
      })
    }
    // eslint-disable-next-line
  },[getCenterList, personalList]);
  return (
    <div style={{height:'100vh'}}>
      {
        personalList && 
        <div style={{height:'20%', width:'100%', backgroundColor:'skyblue', display:'flex', alignItems:'center', justifyContent:'flex-start', padding:'1rem'}}>
          <Avatar style={{width:'4.5rem',height:'4.5rem', backgroundColor:'white'}}
            src={
              <Image
                src="https://joeschmoe.io/api/v1/random"
                style={{
                  width: '100%',
                }}
              />
            }
          />
          <span 
            style={
              {
                marginLeft:'1rem', 
                color:'white', 
                fontWeight:'bold',
                fontSize:'1.5rem',
                flex:1
              }
            }>
              {username}
          </span>
          <span style={{width:'5rem', paddingTop:'.5rem', color:'white', fontWeight:'bold'}} onClick={()=>{
            navigate('/auth/Changecenter')
          }}>编辑个人资料</span>
        </div>
      }
      {
        personalList &&
        <Form layout='horizontal' ref={ref}>
          <Form.Item name='gender' label='性别' disabled>
            <Input/>
          </Form.Item>
          <Form.Item name='country' label='国籍' disabled>
            <Input/>
          </Form.Item>
          <Form.Item  label='语言' disabled>
            <span>中文</span>
          </Form.Item>
          <Form.Item name='city' label='城市' disabled>
            <Input/>
          </Form.Item>
          <Form.Item name='tel' label='电话' disabled>
            <Input/>
          </Form.Item>
          <Form.Item name='address' label='地址' disabled>
            <Input/>
          </Form.Item>
        </Form>
      }
      <Button 
        style={{position: 'fixed', bottom:'3.1625rem'}} 
        block 
        color='danger' 
        size='large'
        onClick={()=>{
          navigate('/login');
        }}
      >
        退出登录
      </Button>
    </div>
  )
}

const mapStateToProps = (state) =>{
  return {
    personalList: state.centerReducer.personalList
  }
}

const mapDispatchToProps = {
  getCenterList,
}

export default connect(mapStateToProps, mapDispatchToProps)(Center);