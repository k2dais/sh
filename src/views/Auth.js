import React from 'react'
import { Outlet, useLocation, useNavigate } from 'react-router-dom'
import { TabBar } from 'antd-mobile'
import {
  AppOutline,
  ShopbagOutline,
  AppstoreOutline,
  UserOutline,
} from 'antd-mobile-icons'
import { connect } from 'react-redux';

function Auth(props) {
  let {isShow} = props;
  const location = useLocation()
  const navigate = useNavigate()
  const { pathname } = location
  const setRouteActive = (value) => {
    navigate(value)
  }
  const tabs = [
    {
      key: '/auth/home',
      title: '首页',
      icon: <AppOutline />,
    },
    {
      key: '/auth/category',
      title: '分类',
      icon: <AppstoreOutline />,
    },
    {
      key: '/auth/shopping',
      title: '购物袋',
      icon: <ShopbagOutline />,
    },
    {
      key: '/auth/center',
      title: '个人中心',
      icon: <UserOutline />,
    },
  ]

  return (
    <div>
      <Outlet></Outlet>
      {
        isShow &&
        <TabBar  style={{bottom:0, position:'fixed', width:'100%', backgroundColor:'white'}} activeKey={pathname} onChange={value => setRouteActive(value)}>
          {tabs.map(item => (
            <TabBar.Item key={item.key} icon={item.icon} title={item.title}/>
          ))}
        </TabBar>
      }
    </div>
  )
}

const mapStateToProps = (state) =>{
  return {
    isShow: state.tabbarReducer.tabbarShow
  }
}

export default connect(mapStateToProps)(Auth);