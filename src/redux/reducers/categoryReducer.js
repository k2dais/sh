
const categoryReducer = (oldstate={index:0}, action) =>{
    let newstate = {...oldstate};
    switch(action.type){
        case 'change-index':
            newstate.index = action.value;
            return newstate;
        default:
            return oldstate;
    }
}

export default categoryReducer;