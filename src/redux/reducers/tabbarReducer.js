
function tabbarReducer(oldstate={tabbarShow: true}, action){
    let newstate = {...oldstate};
    switch(action.type){
        case 'tabbar-show':
            newstate.tabbarShow = true;
            return newstate;
        case 'tabbar-hidden':
            newstate.tabbarShow = false;
            return newstate;
        default:
            return oldstate;
    }
}

export default tabbarReducer;