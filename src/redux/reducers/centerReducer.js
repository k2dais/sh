
const centerReducer = (oldstate={personalList:[]}, action) =>{
    let newstate = {...oldstate};
    switch(action.type){
        case 'getpersonallist':
            newstate.personalList = action.value;
            return newstate;
        default:
            return oldstate;
    }
}

export default centerReducer;