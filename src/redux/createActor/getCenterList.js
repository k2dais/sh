import axios from "axios";

let { username, password } = JSON.parse(localStorage.getItem('token'));

const getCenterList = () => {
    return (dispatch) =>{
        axios(`/users?username=${username}&password=${password}`).then(res =>{
            dispatch({
                type: 'getpersonallist',
                value: res.data
            })
        })
    }
}

export default getCenterList;