import { applyMiddleware, combineReducers, legacy_createStore as createStore } from "redux";
import tabbarReducer from './reducers/tabbarReducer'
import categoryReducer from "./reducers/categoryReducer";
import centerReducer from "./reducers/centerReducer";
import {persistStore, persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import reduxThunk from 'redux-thunk'

const persistConfig = {
    key: 'root',
    storage,
    blacklist:['tabbarReducer', 'centerReducer']  //设置某个reducer数据不持久化，
  }

const reducers = combineReducers({
    tabbarReducer,
    categoryReducer,
    centerReducer
})

const myPersistReducer = persistReducer(persistConfig, reducers)
const store = createStore(myPersistReducer, applyMiddleware(reduxThunk));
const persistor = persistStore(store);

export default store;
export {persistor};