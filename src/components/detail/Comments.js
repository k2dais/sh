import axios from 'axios';
import React, { useEffect, useState } from 'react'
import '../css/Comments.css'

export default function Comments() {
  const [commentList, setCommentList] = useState();
  useEffect(()=>{
    axios('/comments').then(res=>{
      setCommentList(res.data);
    })
  },[])
  return (
    <div style={{paddingBottom:'3.0625rem'}}>
      {commentList && commentList.map(item=>
          <div style={{padding:'1rem', borderBottom:'1px solid lightgrey'}} key={item.imgUrl}>
            <div className='commentitem'>
              <img style={{width:'3rem', borderRadius:'50%'}} src={`http://localhost:5000/${item.userImageUrl}`} alt='加载失败' />
              <span style={{flex:1, marginLeft:'1rem'}}>{item.nickname}</span>
              <span>{item.creationTime}</span>
            </div>
            <div className='commentitem' style={{margin:'1rem 0'}}>{item.content}</div>
            <div className='commentitem'>
              <img style={{width:'6rem'}} src={`http://localhost:5000/${item.imgUrl}`} alt='加载失败'/>
            </div>
          </div>
      )}
    </div>
  )
}
