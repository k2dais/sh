import React from 'react'
import '../css/Goodsdetail.css'

export default function Goodsdetail(props) {
  let {goodlist} = props;
  return (
    <div style={{paddingBottom:'3.0625rem'}}>
      {goodlist && 
        <div style={{contentPadding:'0'}}>
          <div style={{fontSize:'1.3rem', padding:'.75rem'}}>{goodlist.title}</div>
          <div style={{color:'grey', padding:'.75rem'}}>{goodlist.feature}</div>
          {goodlist.desc.map(item=>
            <img key={item} style={{width:'100%', display:'block'}} src={item} alt='加载失败'/>)}
        </div>}

    </div>
  )
}
