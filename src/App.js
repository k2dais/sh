import MyRouter from './router/Index'
import './util/http'

function App() {
  return (
    <div className="App">
      <MyRouter></MyRouter>
    </div>
  );
}

export default App;
